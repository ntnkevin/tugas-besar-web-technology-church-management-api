const { sha256, jwt } = require("../utils");
const uuid = require('uuid/v4');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      id: {
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: uuid(),
        allowNull: false,
      },
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      role: DataTypes.STRING,
      password: DataTypes.STRING,
      rawPassword: {
        type: DataTypes.VIRTUAL,
        set(val) {
          this.setDataValue("rawPassword", val);
          this.setDataValue("password", sha256(val));
        },
      },
    },
    {},
  );
  // User.associate = function associate(models) {
  // associations can be defined here
  // };
  Object.assign(User.prototype, {
    authenticationData,
  });

  User.hashPassword = function hashPassword(value) {
    return sha256(value);
  };

  User.prototype.generateToken = function generateToken() {
    return jwt.encode({
      email: this.email,
      id: this.id,
      password: this.password,
    });
  };

  function authenticationData() {
    return {
      id: this.id,
      email: this.email,
      password: this.password,
    };
  }

  User.beforeCreate(user => user.id = uuid());
  
  return User;
};
