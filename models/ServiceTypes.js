const uuid = require('uuid/v4');
module.exports = (sequelize, DataTypes) => {
  const ServiceTypes = sequelize.define('ServiceType', {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    type: DataTypes.STRING,
  }, {});

  ServiceTypes.associate = function(models) {
    // associations can be defined here
    ServiceTypes.hasMany( models.Service, {
      foreignKey: 'type',
      as: 'service'
    } )
  };
 
  return ServiceTypes;
};