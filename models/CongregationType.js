module.exports = (sequelize, DataTypes) => {
  const CongregationType = sequelize.define('CongregationType', {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    type: DataTypes.STRING,
  }, {});

  CongregationType.associate = function(models) {
    // associations can be defined here
    CongregationType.hasMany( models.Congregation, {
      foreignKey: 'type'
    } )
  };

  return CongregationType;
};