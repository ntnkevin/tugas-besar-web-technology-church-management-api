module.exports = (sequelize, DataTypes) => {
  const Services = sequelize.define('Service', {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    title: DataTypes.STRING,
    type: DataTypes.UUID,
    dateandtime: DataTypes.STRING
  }, {});
  Services.associate = function(models) {
    // associations can be defined here
    // Services.belongsToMany( models.Congregation, {
    //   foreignKey: 'serviceid'
    // });

    Services.belongsTo( models.ServiceType, {
      foreignKey: 'type'
    } )
  };
  
  return Services;
};