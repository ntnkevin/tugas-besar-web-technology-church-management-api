module.exports = (sequelize, DataTypes) => {
  const Congregations = sequelize.define('Congregation', {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    fullname: DataTypes.STRING,
    address: DataTypes.STRING,
    birthdate: DataTypes.STRING,
    type: DataTypes.STRING
  }, {});
  Congregations.associate = function(models) {
    // associations can be defined here
    // Congregations.belongsToMany( models.CongregationService, {
    //   foreignKey: 'congregationid'
    // })
    Congregations.belongsTo( models.CongregationType, {
      foreignKey: 'type'
    } )
  };

  return Congregations;
};