module.exports = (sequelize, DataTypes) => {
  const CongregationService = sequelize.define('CongregationService', {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    serviceid: DataTypes.UUID,
    congregationid: DataTypes.UUID,
    role: DataTypes.STRING
  }, {});
  CongregationService.associate = function(models) {
    // associations can be defined here
    CongregationService.belongsTo( models.Congregation, {
      foreignKey: 'congregationid'
    })
    
    CongregationService.belongsTo( models.Service, {
      foreignKey: 'serviceid'
    })
    CongregationService.belongsTo( models.CongregationServiceRole ,{
      foreignKey: 'role'
    } )
  };

  return CongregationService;
};