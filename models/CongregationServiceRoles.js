module.exports = (sequelize, DataTypes) => {
  const CongregationServiceRoles = sequelize.define('CongregationServiceRole', {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    role: DataTypes.STRING,
  }, {});

  CongregationServiceRoles.associate = function(models) {
    // associations can be defined here
    CongregationServiceRoles.hasMany( models.CongregationService, {
      foreignKey: 'role'
    } )
  };

  CongregationServiceRoles.beforeCreate(congregationserviceroles => congregationserviceroles.id = uuid());

  return CongregationServiceRoles;
};