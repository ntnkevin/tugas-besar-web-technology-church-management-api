const CongregationTypeFacade = require("../facades/CongregationTypeFacade");

module.exports = {
  list,
  get,
  insert,
  update,
  delete: del,
  info,
};

async function list(req) {
  const { query } = req;
  const { filter, options } = _parseFilterOptions(query);
  return CongregationTypeFacade.list(filter, options);
}

async function get(req) {
  const { id } = req.params;
  return CongregationTypeFacade.get(id);
}

async function insert(req) {
  return CongregationTypeFacade.insert(req.body);
}

async function update(req) {
  const { id } = req.params;
  return CongregationTypeFacade.update(id, req.body);
}

async function del(req) {
  const { id } = req.params;
  return CongregationTypeFacade.delete(id);
}

async function info(req) {
  const { query } = req;
  const { filter, options } = _parseFilterOptions(query);
  return CongregationTypeFacade.info(filter, options);
}

function _parseFilterOptions(query) {
  return {
    filter: {},
    options: {},
  };
}
