const UserFacade = require("../facades/TestFacade");

module.exports = {
  list,
  get,
  insert,
  update,
  delete: del,
  info,
};

async function list(req) {
  const { query } = req;
  const { filter, options } = _parseFilterOptions(query);
  return UserFacade.list(filter, options);
}

async function get(req) {
  const { id } = req.params;
  return UserFacade.get(id);
}

async function insert(req) {
  return UserFacade.insert(req.body);
}

async function update(req) {
  const { id } = req.params;
  return UserFacade.update(id, req.body);
}

async function del(req) {
  const { id } = req.params;
  return UserFacade.delete(id);
}

async function info(req) {
  const { query } = req;
  const { filter, options } = _parseFilterOptions(query);
  return UserFacade.info(filter, options);
}

function _parseFilterOptions(query) {
  return {
    filter: {},
    options: {},
  };
}
