const CongregationServiceFacade = require("../facades/CongregationServiceFacade");

module.exports = {
  list,
  get,
  insert,
  update,
  delete: del,
  info,
};

async function list(req) {
  const { query } = req;
  const { filter, options } = _parseFilterOptions(query);
  return CongregationServiceFacade.list(filter, options);
}

async function get(req) {
  const { id } = req.params;
  return CongregationServiceFacade.get(id);
}

async function insert(req) {
  return CongregationServiceFacade.insert(req.body);
}

async function update(req) {
  const { id } = req.params;
  return CongregationServiceFacade.update(id, req.body);
}

async function del(req) {
  const { id } = req.params;
  return CongregationServiceFacade.delete(id);
}

async function info(req) {
  const { query } = req;
  const { filter, options } = _parseFilterOptions(query);
  return CongregationServiceFacade.info(filter, options);
}

function _parseFilterOptions(query) {
  return {
    filter: {},
    options: {},
  };
}
