const CongregationServiceRoleFacade = require("../facades/CongregationServiceRoleFacade");

module.exports = {
  list,
  get,
  insert,
  update,
  delete: del,
  info,
};

async function list(req) {
  const { query } = req;
  const { filter, options } = _parseFilterOptions(query);
  return CongregationServiceRoleFacade.list(filter, options);
}

async function get(req) {
  const { id } = req.params;
  return CongregationServiceRoleFacade.get(id);
}

async function insert(req) {
  return CongregationServiceRoleFacade.insert(req.body);
}

async function update(req) {
  const { id } = req.params;
  return CongregationServiceRoleFacade.update(id, req.body);
}

async function del(req) {
  const { id } = req.params;
  return CongregationServiceRoleFacade.delete(id);
}

async function info(req) {
  const { query } = req;
  const { filter, options } = _parseFilterOptions(query);
  return CongregationServiceRoleFacade.info(filter, options);
}

function _parseFilterOptions(query) {
  return {
    filter: {},
    options: {},
  };
}
