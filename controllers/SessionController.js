const { SessionFacade } = require("../facades");
const { render } = require("../utils");

module.exports = {
  login,
  info,
  changePassword,
};

async function login(request) {
  const { email, password } = request.body;
  const result = await SessionFacade.login(email, password);
  return render("login", result);
}

async function changePassword(request) {
  const { oldPassword, newPassword } = request.body;
  const { user } = request.context;
  const result = await SessionFacade.changePassword(
    user.id,
    oldPassword,
    newPassword,
  );
  return result;
}

async function info(request) {
  const { user } = request.context;
  return render("me", user);
}
