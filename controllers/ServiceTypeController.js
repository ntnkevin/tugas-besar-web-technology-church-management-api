const ServiceTypeFacade = require("../facades/ServiceTypeFacade");

module.exports = {
  list,
  get,
  insert,
  update,
  delete: del,
  info,
};

async function list(req) {
  const { query } = req;
  const { filter, options } = _parseFilterOptions(query);
  return ServiceTypeFacade.list(filter, options);
}

async function get(req) {
  const { id } = req.params;
  return ServiceTypeFacade.get(id);
}

async function insert(req) {
  return ServiceTypeFacade.insert(req.body);
}

async function update(req) {
  const { id } = req.params;
  return ServiceTypeFacade.update(id, req.body);
}

async function del(req) {
  const { id } = req.params;
  return ServiceTypeFacade.delete(id);
}

async function info(req) {
  const { query } = req;
  const { filter, options } = _parseFilterOptions(query);
  return ServiceTypeFacade.info(filter, options);
}

function _parseFilterOptions(query) {
  return {
    filter: {},
    options: {},
  };
}
