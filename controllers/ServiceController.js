const ServiceFacade = require("../facades/ServiceFacade");

module.exports = {
  list,
  get,
  insert,
  update,
  delete: del,
  info,
};

async function list(req) {
  const { query } = req;
  const { filter, options } = _parseFilterOptions(query);
  return ServiceFacade.list(filter, options);
}

async function get(req) {
  const { id } = req.params;
  return ServiceFacade.get(id);
}

async function insert(req) {
  return ServiceFacade.insert(req.body);
}

async function update(req) {
  const { id } = req.params;
  return ServiceFacade.update(id, req.body);
}

async function del(req) {
  const { id } = req.params;
  return ServiceFacade.delete(id);
}

async function info(req) {
  const { query } = req;
  const { filter, options } = _parseFilterOptions(query);
  return ServiceFacade.info(filter, options);
}

function _parseFilterOptions(query) {
  return {
    filter: {},
    options: {},
  };
}
