const RootController = require("./RootController");
const SessionController = require("./SessionController");
const UserController = require("./UserController");
const CongregationController = require("./CongregationController");
const CongregationTypeController = require("./CongregationTypeController");
const CongregationServiceController = require("./CongregationServiceController");
const CongregationServiceRoleController = require("./CongregationServiceRoleController");
const ServiceController = require("./ServiceController");
const ServiceTypeController = require("./ServiceTypeController");

module.exports = {
  UserController,
  SessionController,
  RootController,
  CongregationController,
  CongregationTypeController,
  CongregationServiceController,
  CongregationServiceRoleController,
  ServiceController,
  ServiceTypeController,
};

// const getTableData = (req, res, db) => {
//     db.select('*').from('Congregations')
//       .then(items => {
//         if(items.length){
//           res.json(items)
//         } else {
//           res.json({dataExists: 'false'})
//         }
//       })
//       .catch(err => res.status(400).json({dbError: 'db error'}))
//   }
  
//   const postTableData = (req, res, db) => {
//     const { id,fullName, address, birthDate, type } = req.body
//     db('Congregations').insert({ id,fullName, address, birthDate, type })
//       .returning('*')
//       .then(item => {
//         res.json(item)
//       })
//       .catch(err => res.status(400).json({dbError: 'db error'}))
//   }
  
//   const putTableData = (req, res, db) => {
//     const { id, fullName, address, birthDate, type } = req.body
//     db('Congregations').where({id}).update({fullName, address, birthDate, type})
//       .returning('*')
//       .then(item => {
//         res.json(item)
//       })
//       .catch(err => res.status(400).json({dbError: 'db error'}))
//   }
  
//   const deleteTableData = (req, res, db) => {
//     const { id } = req.body
//     db('Congregations').where({id}).del()
//       .then(() => {
//         res.json({delete: 'true'})
//       })
//       .catch(err => res.status(400).json({dbError: 'db error'}))
//   }
  
//   module.exports = {
//     getTableData,
//     postTableData,
//     putTableData,
//     deleteTableData
//   }