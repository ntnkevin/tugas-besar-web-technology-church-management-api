const CongregationFacade = require("../facades/CongregationFacade");

module.exports = {
  list,
  get,
  insert,
  update,
  delete: del,
  info,
};

async function list(req) {
  const { query } = req;
  const { filter, options } = _parseFilterOptions(query);
  return CongregationFacade.list(filter, options);
}

async function get(req) {
  const { id } = req.params;
  return CongregationFacade.get(id);
}

async function insert(req) {
  return CongregationFacade.insert(req.body);
}

async function update(req) {
  const { id } = req.params;
  return CongregationFacade.update(id, req.body);
}

async function del(req) {
  const { id } = req.params;
  return CongregationFacade.delete(id);
}

async function info(req) {
  const { query } = req;
  const { filter, options } = _parseFilterOptions(query);
  return CongregationFacade.info(filter, options);
}

function _parseFilterOptions(query) {
  return {
    filter: {},
    options: {},
  };
}
