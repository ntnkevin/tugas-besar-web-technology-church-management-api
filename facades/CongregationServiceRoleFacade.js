const { NotFoundException } = require("../exceptions");

const { CongregationServiceRole } = require("../models");

module.exports = {
  list,
  get,
  insert,
  update,
  delete: del,
  info,
};

async function list(filter, options) {
  const records = await CongregationServiceRole.findAll({
    where: filter,
    ...options,
  });
  return records;
}

async function get(id) {
  const record = await CongregationServiceRole.findByPk(id);
  if (!record) { throw new NotFoundException(id, "CongregationServiceRole"); }
  return record;
}

async function insert(data) {
  await CongregationServiceRole.create(data);
  return {
    success: true,
  };
}

async function update(id, data) {
  const record = await get(id);
  await record.update(data);
  return {
    success: true,
  };
}

async function del(id) {
  const record = await get(id);
  await record.destroy();
  return {
    success: true,
  };
}

async function info(filter, options) {
  const count = await CongregationServiceRole.count({
    where: filter,
  });
  return {
    count,
  };
}
