const { NotFoundException } = require("../exceptions");

const { Test } = require("../models");

module.exports = {
  list,
  get,
  insert,
  update,
  delete: del,
  info,
};

async function list(filter, options) {
  const records = await Test.findAll({
    where: filter,
    ...options,
  });
  return records;
}

async function get(id) {
  const record = await Test.findByPk(id);
  if (!record) { throw new NotFoundException(id, "Test"); }
  return record;
}

async function insert(data) {
  await User.create(data);
  return {
    success: true,
  };
}

async function update(id, data) {
  const record = await get(id);
  await record.update(data);
  return {
    success: true,
  };
}

async function del(id) {
  const record = await get(id);
  await record.destroy();
  return {
    success: true,
  };
}

async function info(filter, options) {
  const count = await Test.count({
    where: filter,
  });
  return {
    count,
  };
}
