const { NotFoundException } = require("../exceptions");

const { Congregation, Service, CongregationService, CongregationServiceRole } = require("../models");

module.exports = {
  list,
  get,
  insert,
  update,
  delete: del,
  info,
};

async function list(filter, options) {
  const records = await CongregationService.findAll({
    where: filter,
    include: [{
      model: Congregation
    },
    {
      model: Service
    },
    {
      model: CongregationServiceRole
    }],
    ...options,
  });
  return records;
}

async function get(id) {
  const record = await CongregationService.findByPk(id);
  if (!record) { throw new NotFoundException(id, "CongregationService"); }
  return record;
}

async function insert(data) {
  await CongregationService.create(data);
  return {
    success: true,
  };
}

async function update(id, data) {
  const record = await get(id);
  await record.update(data);
  return {
    success: true,
  };
}

async function del(id) {
  const record = await get(id);
  await record.destroy();
  return {
    success: true,
  };
}

async function info(filter, options) {
  const count = await CongregationService.count({
    where: filter,
  });
  return {
    count,
  };
}
