const { NotFoundException } = require("../exceptions");

const { CongregationType } = require("../models");

module.exports = {
  list,
  get,
  insert,
  update,
  delete: del,
  info,
};

async function list(filter, options) {
  const records = await CongregationType.findAll({
    where: filter,
    ...options,
  });
  return records;
}

async function get(id) {
  const record = await CongregationType.findByPk(id);
  if (!record) { throw new NotFoundException(id, "CongregationType"); }
  return record;
}

async function insert(data) {
  await CongregationType.create(data);
  return {
    success: true,
  };
}

async function update(id, data) {
  const record = await get(id);
  await record.update(data);
  return {
    success: true,
  };
}

async function del(id) {
  const record = await get(id);
  await record.destroy();
  return {
    success: true,
  };
}

async function info(filter, options) {
  const count = await CongregationType.count({
    where: filter,
  });
  return {
    count,
  };
}
