const { User } = require("../models");
const {
  RequiredException,
  LoginFailedException,
  NotFoundException,
  WrongPasswordException,
} = require("../exceptions");
const { jwt } = require("../utils");

module.exports = {
  getUserByAuthentication,
  login,
  changePassword,
};

async function changePassword(userId, oldPassword, newPassword) {
  const user = await User.findByPk(userId);
  if (!user) {
    throw new NotFoundException(userId, "User");
  }
  if (user.password !== User.hashPassword(oldPassword)) {
    throw new WrongPasswordException();
  }
  const updatedUser = await user.update({
    rawPassword: newPassword,
  });
  return {
    success: true,
    token: updatedUser.generateToken(),
  };
}

async function getUserByAuthentication(authentication) {
  if (!authentication) return null;
  const { id, email, password } = authentication;
  if (!id || !password || !email) return null;
  return User.findOne({
    where: {
      id,
      email,
      password,
    },
  });
}

async function login(email, rawPassword) {
  RequiredException.check("email", email);
  RequiredException.check("rawPassword", rawPassword);
  const user = await User.findOne({
    where: {
      email,
      password: User.hashPassword(rawPassword),
    },
  });
  if (!user) throw new LoginFailedException();
  return {
    token: user.generateToken(),
  };
}
