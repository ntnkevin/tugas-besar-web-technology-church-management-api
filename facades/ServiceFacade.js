const { NotFoundException } = require("../exceptions");

const { Service, ServiceType } = require("../models");

module.exports = {
  list,
  get,
  insert,
  update,
  delete: del,
  info,
};

async function list(filter, options) {
  const records = await Service.findAll({
    where: filter,
    include: [{
      model: ServiceType
    }],
    ...options,
  });
  return records;
}

async function get(id) {
  const record = await Service.findByPk(id);
  if (!record) { throw new NotFoundException(id, "Service"); }
  return record;
}

async function insert(data) {
  await Service.create(data);
  return {
    success: true,
  };
}

async function update(id, data) {
  const record = await get(id);
  await record.update(data);
  return {
    success: true,
  };
}

async function del(id) {
  const record = await get(id);
  await record.destroy();
  return {
    success: true,
  };
}

async function info(filter, options) {
  const count = await Service.count({
    where: filter,
  });
  return {
    count,
  };
}
