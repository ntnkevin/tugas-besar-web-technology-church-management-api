const { NotFoundException } = require("../exceptions");

const { Congregation, CongregationType } = require("../models");

module.exports = {
  list,
  get,
  insert,
  update,
  delete: del,
  info,
};

async function list(filter, options) {
  const records = await Congregation.findAll({
    where: filter,
    include: [{ 
      model: CongregationType
    }],
    ...options,
  });
  return records;
}

async function get(id) {
  const record = await Congregation.findByPk(id);
  if (!record) { throw new NotFoundException(id, "Congregation"); }
  return record;
}

async function insert(data) {
  await Congregation.create(data);
  return {
    success: true,
  };
}

async function update(id, data) {
  const record = await get(id);
  await record.update(data);
  return {
    success: true,
  };
}

async function del(id) {
  const record = await get(id);
  await record.destroy();
  return {
    success: true,
  };
}

async function info(filter, options) {
  const count = await Congregation.count({
    where: filter,
  });
  return {
    count,
  };
}
