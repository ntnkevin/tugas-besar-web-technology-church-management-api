const {
  Router,
} = require("../modules");
const {
  RootController,
  SessionController,
  UserController,
  CongregationController,
  CongregationTypeController,
  CongregationServiceController,
  CongregationServiceRoleController,
  ServiceController,
  ServiceTypeController,
} = require("../controllers");

const router = new Router();
const id = new String();

// router.get("/", (req, res) => res.send("hello world"));
// router.get("/crud", (req, res) => index.getTableData(req, res, db));
// router.post("/crud", (req, res) => index.postTableData(req, res, db));
// router.put("/crud", (req, res) => index.putTableData(req, res, db));
// router.delete("/crud", (req, res) => main.deleteTableData(req, res, db));
router.get("/info", RootController.info);
router.post("/login", SessionController.login);
router.get("/me", SessionController.info);
router.post("/change-password", SessionController.changePassword);

router.rest("/user", UserController);
router.rest("/congregation", CongregationController);
router.rest("/congregationtype", CongregationTypeController);
// router.rest("/congregationtype/update", CongregationTypeController.update(id));
// router.rest("/congregationtype/delete", CongregationTypeController.delete(id));
// router.get("/congregationtype/get/", CongregationTypeController.get());
router.rest("/congregationservice", CongregationServiceController);
router.rest("/congregationservicerole", CongregationServiceRoleController);
router.rest("/service", ServiceController);
router.rest("/servicetype",ServiceTypeController);

module.exports = router;
